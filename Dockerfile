FROM ubuntu:20.04
RUN mkdir projects
ENV TZ=America/La_Paz
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y \
    curl \
    git \
    npm \
    iptables
RUN curl -sL https://deb.nodesource.com/setup_16.x | bash -    
RUN apt-get install -y nodejs
RUN npm install -g @angular/cli@13.0.0
EXPOSE 4200/tcp
EXPOSE 4200/udp
CMD [ "/bin/bash" ]